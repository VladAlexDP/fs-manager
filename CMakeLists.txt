cmake_minimum_required(VERSION 2.8)
project(fs-manager CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(PROJECT_PATH "${CMAKE_CURRENT_SOURCE_DIR}")
set(SOURCE_PATH "${PROJECT_PATH}/src")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} \
        -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wformat=2 \
        -Winit-self -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept \
        -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow \
        -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel \
        -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unuse")

if((NOT CMAKE_BUILD_TYPE) OR (CMAKE_BUILD_TYPE STREQUAL "Debug"))
    message(STATUS "building debug version")
    set(CMAKE_BUILD_TYPE Debug)
elseif(CMAKE_BUILD_TYPE STREQUAL "Release")
    message(STATUS "building release version")
    set(CMAKE_BUILD_TYPE Release)
endif((NOT CMAKE_BUILD_TYPE) OR (CMAKE_BUILD_TYPE STREQUAL "Debug"))

set(CMAKE_CXX_FLAGS_DEBUG "-O0 -ggdb")
set(CMAKE_CXX_FLAGS_RELEASE "-O2")

include_directories(${SOURCE_PATH})
add_subdirectory(${SOURCE_PATH})
