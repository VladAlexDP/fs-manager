#pragma once

#include <string>
#include <memory>
#include <vector>

#include "entry.hpp"
#include "visitor.hpp"

namespace entry
{

class Directory: public IEntry
{
public:
    Directory(const std::string &name, std::vector<std::unique_ptr<IEntry>> entries);

    void accept(IVisitor *visitor) override;
    std::string_view get_name() const override;

    void add_child(std::unique_ptr<IEntry> entry);
    void remove(IEntry *entry);
    IEntry *get_child(size_t index);

private:
    std::string m_name;
    std::vector<std::unique_ptr<IEntry>> m_entries;
};

}
