#pragma once

#include <stdint.h>

#include "entry/visitor.hpp"

namespace entry::visitor
{

class TotalSizeVisitor: public IVisitor
{
public:
    void visit_file(File *file) override;
    void visit_directory(Directory *dir) override;

    uint64_t get_result() const;

private:
    uint64_t m_total_size;
};

}
