#include "printing_visitor.hpp"

#include <iostream>

#include "entry/file.hpp"
#include "entry/directory.hpp"

#include "total_size_visitor.hpp"

namespace entry::visitor
{

void PrintingVisitor::visit_file(File *file)
{
    std::cout << "\t" << file->get_name() << " " << file->get_size() << "B\n";
}

void PrintingVisitor::visit_directory(Directory *dir)
{
    auto total_size_visitor = std::make_unique<TotalSizeVisitor>();
    dir->accept(total_size_visitor.get());
    std::cout << dir->get_name() << " " << total_size_visitor->get_result() << "B:\n";
}

}
