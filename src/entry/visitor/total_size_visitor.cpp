#include "total_size_visitor.hpp"

#include "entry/file.hpp"
#include "entry/directory.hpp"

namespace entry::visitor
{

void TotalSizeVisitor::visit_file(File *file)
{
    m_total_size += file->get_size();
}

void TotalSizeVisitor::visit_directory(Directory *)
{
}

uint64_t TotalSizeVisitor::get_result() const
{
    return m_total_size;
}

}
