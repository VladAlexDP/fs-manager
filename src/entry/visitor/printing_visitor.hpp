#pragma once

#include "entry/visitor.hpp"

namespace entry::visitor
{

class PrintingVisitor: public IVisitor
{
public:
    void visit_file(File *file) override;
    void visit_directory(Directory *dir) override;
};

}
