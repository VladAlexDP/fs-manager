#pragma once

#include <string_view>

#include "visitor.hpp"

namespace entry
{

class IEntry
{
public:
    virtual ~IEntry() = default;

    virtual void accept(IVisitor *visitor) = 0;
    virtual std::string_view get_name() const = 0;
};

}
