#pragma once

#include <string>

#include "entry.hpp"
#include "visitor.hpp"

namespace entry
{

class File: public IEntry
{
public:
    File(const std::string &name, uint64_t size);

    void accept(IVisitor *visitor) override;
    std::string_view get_name() const override;

    uint64_t get_size() const;

private:
    std::string m_name;
    uint64_t m_size;
};

}
