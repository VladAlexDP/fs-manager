#include "directory.hpp"

#include <algorithm>

namespace entry
{

Directory::Directory(const std::string &name, std::vector<std::unique_ptr<IEntry>> entries)
    : m_name(name), m_entries(std::move(entries))
{
}

void Directory::accept(IVisitor *visitor)
{
    visitor->visit_directory(this);

    for (auto &e: m_entries) {
        e->accept(visitor);
    }
}

std::string_view Directory::get_name() const
{
    return m_name;
}

void Directory::add_child(std::unique_ptr<IEntry> entry)
{
    m_entries.push_back(std::move(entry));
}

void Directory::remove(IEntry *entry)
{
    auto it = std::find_if(std::begin(m_entries), std::end(m_entries), [&entry](auto &e) {
                               return e.get() == entry;
                           });
    if (it == std::end(m_entries)) {
        throw std::runtime_error("can't remove entry from dir - no such entry");
    }
    m_entries.erase(it);
}

IEntry *Directory::get_child(size_t index)
{
    return m_entries[index].get();
}

}
