#pragma once

namespace entry
{

class File;
class Directory;

class IVisitor
{
public:
    virtual ~IVisitor() = default;

    virtual void visit_file(File *file) = 0;
    virtual void visit_directory(Directory *dir) = 0;
};

}
