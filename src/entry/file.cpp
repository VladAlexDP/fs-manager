#include "file.hpp"

namespace entry
{

File::File(const std::string &name, uint64_t size)
    : m_name(name), m_size(size)
{
}

void File::accept(IVisitor *visitor)
{
    visitor->visit_file(this);
}

std::string_view File::get_name() const
{
    return m_name;
}

uint64_t File::get_size() const
{
    return m_size;
}

}
