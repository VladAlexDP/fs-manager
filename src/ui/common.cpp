#include "common.hpp"

#include <iostream>

namespace ui::common
{

size_t choose(std::string_view message, const std::vector<std::string> &options)
{
    std::cout << ">>> " << message << ":\n";
    for (size_t i = 0; i < options.size(); ++i) {
        std::cout << i + 1 << ": " << options[i] << "\n";
    }

    size_t chosen = 0;
    do {
        std::cin >> chosen;
    } while (chosen == 0 || chosen > options.size());

    return chosen - 1;
}

}
