#include "partition_dialog.hpp"

#include <iostream>

#include "fs/filesystem_ops/ext4_ops.hpp"
#include "fs/filesystem_ops/ntfs_ops.hpp"
#include "fs/filesystem_ops/fat_ops.hpp"

#include "entry/visitor/printing_visitor.hpp"

#include "ui/common.hpp"

namespace ui::ui_state
{

PartitionDialog::PartitionDialog(block_layer::Partition &partition)
    : m_partition(partition)
{
}

void handle_format(block_layer::Partition &partition);
void handle_mount(block_layer::Partition &partition);
void handle_journal(block_layer::Partition &partition);

std::unique_ptr<IUIState> PartitionDialog::handle()
{
    std::vector<std::string> options;

    options.emplace_back("Format");
    options.emplace_back("Mount and display");
    options.emplace_back("Show journal");
    options.emplace_back("Back");

    size_t chosen = ui::common::choose("Choose the next action with partition", options);

    switch (chosen) {
        case 0:
            handle_format(m_partition);
            break;
        case 1:
            handle_mount(m_partition);
            break;
        case 2:
            handle_journal(m_partition);
            break;
        default:
            break;
    }

    return nullptr;
}

void handle_format(block_layer::Partition &partition)
{
    std::vector<std::string> options;

    options.emplace_back("EXT4");
    options.emplace_back("NTFS");
    options.emplace_back("FAT");

    size_t chosen = ui::common::choose("Choose filsystem to format", options);

    switch (chosen) {
        case 0:
            partition.format(std::make_unique<fs::filesystem_ops::EXT4Ops>(std::string(partition.get_path())));
            break;
        case 1:
            partition.format(std::make_unique<fs::filesystem_ops::NTFSOps>(std::string(partition.get_path())));
            break;
        case 2:
            partition.format(std::make_unique<fs::filesystem_ops::FATOps>(std::string(partition.get_path())));
            break;
        default:
            throw std::runtime_error("unreachable");
    }
}

void handle_mount(block_layer::Partition &partition)
{
    std::cout << "Mount point path: ";

    std::string mount_point = "";
    std::cin >> mount_point;
    auto root = partition.get_fs()->mount(mount_point);

    auto visitor = std::make_unique<entry::visitor::PrintingVisitor>();
    root.accept(visitor.get());
}

void handle_journal(block_layer::Partition &partition)
{
    for(auto it = partition.get_fs()->get_journal(); it->has_next();) {
        auto item = it->next();
        std::cout << item->action() << " " << item->first_block() << " " << item->last_block() << " ";
        if (item->successful()) {
            std::cout << "ok";
        } else {
            std::cout << "failed";
        }
        std::cout << "\n";
    }
}

}

