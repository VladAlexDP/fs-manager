#include "choose_device.hpp"

#include "ui/common.hpp"
#include "ui/ui_state/block_device_dialog.hpp"

namespace ui::ui_state
{

ChooseDevice::ChooseDevice(std::vector<block_layer::BlockDevice> &block_devices)
    : m_block_devices(block_devices)
{
}

std::unique_ptr<IUIState> ChooseDevice::handle()
{
    std::vector<std::string> options;

    for (auto &block_device: m_block_devices)
    {
        options.emplace_back(block_device.get_path());
    }

    size_t chosen = ui::common::choose("Choose the device", options);
    return std::make_unique<BlockDeviceDialog>(m_block_devices[chosen]);
}

}
