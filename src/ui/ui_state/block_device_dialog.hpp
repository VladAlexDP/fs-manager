#pragma once

#include "block_layer/block_device.hpp"

#include "ui/ui_state.hpp"

namespace ui::ui_state
{

class BlockDeviceDialog: public IUIState
{
public:
    explicit BlockDeviceDialog(block_layer::BlockDevice &block_device);
    std::unique_ptr<IUIState> handle() override;

private:
    block_layer::BlockDevice &m_block_device;
};

}
