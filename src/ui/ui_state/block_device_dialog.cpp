#include "block_device_dialog.hpp"

#include <iostream>

#include "ui/common.hpp"
#include "ui/ui_state/partition_dialog.hpp"

namespace ui::ui_state
{

BlockDeviceDialog::BlockDeviceDialog(block_layer::BlockDevice &block_device)
    : m_block_device(block_device)
{
}

std::unique_ptr<IUIState> BlockDeviceDialog::handle()
{
    std::vector<std::string> options;

    for (auto &part: m_block_device) {
        options.emplace_back(part.get_path());
    }
    options.emplace_back("Add partition");
    options.emplace_back("Back");

    size_t chosen = ui::common::choose("Choose the next action with block device", options);

    if (chosen == options.size() - 2) {
        std::cout << "Partition start: ";
        uint64_t start = 0;
        std::cin >> start;

        std::cout << "Partition size: ";
        uint64_t size = 0;
        std::cin >> size;

        m_block_device.add_partition(start, size);
        return nullptr;
    }

    if (chosen == options.size() - 1) {
        return nullptr;
    }

    auto chosen_index = static_cast<std::vector<block_layer::Partition>::difference_type>(chosen);
    return std::make_unique<PartitionDialog>(*std::next(m_block_device.begin(), chosen_index));
}

}
