#pragma once

#include <vector>

#include "block_layer/block_device.hpp"

#include "ui/ui_state.hpp"

namespace ui::ui_state
{

class ChooseDevice: public IUIState
{
public:
    explicit ChooseDevice(std::vector<block_layer::BlockDevice> &block_devices);
    std::unique_ptr<IUIState> handle() override;

private:
    std::vector<block_layer::BlockDevice> &m_block_devices;
};

}
