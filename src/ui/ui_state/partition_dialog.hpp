#pragma once

#include "block_layer/partition.hpp"

#include "ui/ui_state.hpp"

namespace ui::ui_state
{

class PartitionDialog: public IUIState
{
public:
    explicit PartitionDialog(block_layer::Partition &partition);
    std::unique_ptr<IUIState> handle() override;

private:
    block_layer::Partition &m_partition;
};

}
