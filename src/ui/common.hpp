#pragma once

#include <string_view>
#include <vector>

namespace ui::common
{

size_t choose(std::string_view message, const std::vector<std::string> &options);

}
