#pragma once

namespace ui
{

class IUIState
{
public:
    virtual ~IUIState() = default;

    virtual std::unique_ptr<IUIState> handle() = 0;
};

}
