#include "ext4_journal_entry.hpp"

#include <sstream>

namespace fs::journal_entry
{

EXT4JournalEntry::EXT4JournalEntry(journal_block_tag3_s block)
    : m_block(std::move(block))
{
}

std::string EXT4JournalEntry::action() const
{
    auto pos = m_block.find(' ', 0);

    return m_block.substr(0, pos);
}

uint64_t EXT4JournalEntry::first_block() const
{
    auto pos1 = m_block.find(' ', 0);
    auto pos2 = m_block.find(' ', pos1 + 1);

    return std::stoull(m_block.substr(pos1 + 1, pos2 - pos1));
}

uint64_t EXT4JournalEntry::last_block() const
{
    auto pos1 = m_block.find(' ', 0);
    auto pos2 = m_block.find(' ', pos1 + 1);
    auto pos3 = m_block.find(' ', pos2 + 2);

    return std::stoull(m_block.substr(pos2 + 1, pos3 - pos2));
}

bool EXT4JournalEntry::successful() const
{
    auto pos1 = m_block.find(' ', 0);
    auto pos2 = m_block.find(' ', pos1 + 1);
    auto pos3 = m_block.find(' ', pos2 + 1);
    auto pos4 = m_block.find(' ', pos3 + 1);

    return m_block.substr(pos3 + 1, pos4 - pos3) == "ok";
}

}
