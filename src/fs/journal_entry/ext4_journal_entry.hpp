#pragma once

#include <string>

#include "fs/journal_entry.hpp"

using journal_block_tag3_s = std::string;

namespace fs::journal_entry
{

class EXT4JournalEntry: public JournalEntry
{
public:
    EXT4JournalEntry(journal_block_tag3_s block);

    std::string action() const override;
    uint64_t first_block() const override;
    uint64_t last_block() const override;
    bool successful() const override;

private:
    journal_block_tag3_s m_block;
};

}
