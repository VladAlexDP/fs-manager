#include "ntfs_ops.hpp"

#include "fs/filesystem/ntfs.hpp"

namespace fs::filesystem_ops
{

NTFSOps::NTFSOps(const std::string &part_path)
    : m_part_path(part_path)
{
}

std::unique_ptr<IFilesystem> NTFSOps::detect() const
{
    if (m_part_path == "/dev/sdc1") {
        return std::make_unique<fs::filesystem::NTFS>(m_part_path);
    }

    return nullptr;
}

std::unique_ptr<IFilesystem> NTFSOps::format() const
{
    return std::make_unique<fs::filesystem::NTFS>(m_part_path);
}

}
