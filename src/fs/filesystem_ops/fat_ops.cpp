#include "fat_ops.hpp"

#include "fs/filesystem/fat.hpp"

namespace fs::filesystem_ops
{

FATOps::FATOps(const std::string &part_path)
    : m_part_path(part_path)
{

}

std::unique_ptr<IFilesystem> FATOps::detect() const
{
    if (m_part_path[m_part_path.size() - 1] == '0') {
        return std::make_unique<fs::filesystem::FAT>(m_part_path);
    }

    return nullptr;
}

std::unique_ptr<IFilesystem> FATOps::format() const
{
    return std::make_unique<fs::filesystem::FAT>(m_part_path);
}

}
