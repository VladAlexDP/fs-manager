#include "unformated_ops.hpp"

#include "fs/filesystem/unformated.hpp"

namespace fs::filesystem_ops
{

std::unique_ptr<IFilesystem> UnformatedOps::detect() const
{
    return std::make_unique<fs::filesystem::Unformated>();
}

std::unique_ptr<IFilesystem> UnformatedOps::format() const
{
    return std::make_unique<fs::filesystem::Unformated>();
}

}
