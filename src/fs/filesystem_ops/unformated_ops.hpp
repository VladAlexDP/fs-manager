#pragma once

#include "fs/filesystem_ops.hpp"

namespace fs::filesystem_ops
{

class UnformatedOps: public IFilesystemOps
{
public:
    std::unique_ptr<IFilesystem> detect() const override;
    std::unique_ptr<IFilesystem> format() const override;

private:
    std::string part_path;
};

}
