#pragma once

#include "fs/filesystem_ops.hpp"

namespace fs::filesystem_ops
{

class EXT4Ops: public IFilesystemOps
{
public:
    explicit EXT4Ops(const std::string &part_path);
    std::unique_ptr<IFilesystem> detect() const override;
    std::unique_ptr<IFilesystem> format() const override;

private:
    std::string m_part_path;
};

}
