#include "ext4_ops.hpp"

#include "fs/filesystem/ext4.hpp"

namespace fs::filesystem_ops
{

EXT4Ops::EXT4Ops(const std::string &part_path)
    : m_part_path(part_path)
{

}

std::unique_ptr<IFilesystem> EXT4Ops::detect() const
{
    if (m_part_path == "/dev/sda1") {
        return std::make_unique<fs::filesystem::EXT4>(m_part_path);
    }

    return nullptr;
}

std::unique_ptr<IFilesystem> EXT4Ops::format() const
{
    return std::make_unique<fs::filesystem::EXT4>(m_part_path);
}

}
