#pragma once

#include "fs/filesystem_ops.hpp"

namespace fs::filesystem_ops
{

class NTFSOps: public IFilesystemOps
{
public:
    explicit NTFSOps(const std::string &part_path);
    std::unique_ptr<IFilesystem> detect() const override;
    std::unique_ptr<IFilesystem> format() const override;

private:
    std::string m_part_path;
};

}
