#pragma once

#include "fs/filesystem_ops.hpp"

namespace fs::filesystem_ops
{

class FATOps: public IFilesystemOps
{
public:
    explicit FATOps(const std::string &part_path);
    std::unique_ptr<IFilesystem> detect() const override;
    std::unique_ptr<IFilesystem> format() const override;

private:
    std::string m_part_path;
};

}
