#include "filesystem_detector.hpp"

#include <stdexcept>

#include "filesystem_ops/ntfs_ops.hpp"
#include "filesystem_ops/ext4_ops.hpp"
#include "filesystem_ops/fat_ops.hpp"
#include "filesystem_ops/unformated_ops.hpp"

namespace fs
{

FilesystemDetector::FilesystemDetector(const std::string &partition_path)
    : m_candidate(std::make_unique<filesystem_ops::NTFSOps>(partition_path)),
      m_successor(
            std::make_unique<FilesystemDetector>(
                std::make_unique<filesystem_ops::EXT4Ops>(partition_path),
                std::make_unique<FilesystemDetector>(
                    std::make_unique<filesystem_ops::FATOps>(partition_path),
                    std::make_unique<FilesystemDetector>(
                        std::make_unique<filesystem_ops::UnformatedOps>(),
                        nullptr
                    )
                )
            )
      )
{
}

std::unique_ptr<IFilesystem> FilesystemDetector::detect() const
{
    std::unique_ptr<IFilesystem> res = this->m_candidate->detect();
    if (res != nullptr) {
        return res;
    }

    if (this->m_successor != nullptr) {
        return this->m_successor->detect();
    }

    throw std::runtime_error("unsupported partition table");
}

FilesystemDetector::FilesystemDetector(std::unique_ptr<IFilesystemOps> candidate, std::unique_ptr<FilesystemDetector> successor)
    : m_candidate(std::move(candidate)), m_successor(std::move(successor))
{
}

}
