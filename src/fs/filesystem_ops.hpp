#pragma once

#include <memory>
#include <string>

#include "filesystem.hpp"

namespace fs
{

class IFilesystemOps
{
public:
    virtual ~IFilesystemOps() = default;

    virtual std::unique_ptr<IFilesystem> detect() const = 0;
    virtual std::unique_ptr<IFilesystem> format() const = 0;
};

}
