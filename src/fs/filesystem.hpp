#pragma once

#include <memory>
#include <string>

#include "entry/directory.hpp"

#include "journal_iterator.hpp"

namespace fs
{

class IFilesystem
{
public:
    virtual ~IFilesystem() = default;

    virtual entry::Directory mount(std::string_view path) = 0;
    virtual std::unique_ptr<IJournalIterator> get_journal() = 0;
};

}
