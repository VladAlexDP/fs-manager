#pragma once

#include <memory>
#include <optional>
#include <string_view>

#include "filesystem.hpp"
#include "filesystem_ops.hpp"

namespace fs
{

class FilesystemDetector
{
public:
    FilesystemDetector(const std::string &partition_path);
    FilesystemDetector(std::unique_ptr<IFilesystemOps> candidate, std::unique_ptr<FilesystemDetector> successor);

    std::unique_ptr<IFilesystem> detect() const;

private:
    std::unique_ptr<IFilesystemOps> m_candidate;
    std::unique_ptr<FilesystemDetector> m_successor;
};

}
