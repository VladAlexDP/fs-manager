#pragma once

#include "fs/filesystem.hpp"

namespace fs::filesystem
{

class Unformated: public IFilesystem
{
public:
    entry::Directory mount(std::string_view path) override;
    std::unique_ptr<IJournalIterator> get_journal() override;
};

}
