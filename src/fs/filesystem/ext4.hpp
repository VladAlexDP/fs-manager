#pragma once

#include <string_view>

#include "fs/journal_entry/ext4_journal_entry.hpp"

#include "fs/filesystem.hpp"

namespace fs::filesystem
{

class EXT4: public IFilesystem
{
public:
    explicit EXT4(const std::string &part_path);

    entry::Directory mount(std::string_view path) override;
    std::unique_ptr<IJournalIterator> get_journal() override;

private:
    std::string m_part_path;
    std::vector<std::unique_ptr<fs::journal_entry::EXT4JournalEntry>> m_journal;
};

}
