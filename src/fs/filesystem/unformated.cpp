#include "unformated.hpp"

namespace fs::filesystem
{

entry::Directory Unformated::mount(std::string_view)
{
    throw std::runtime_error("can't mount - filesystem is not formated");
}

std::unique_ptr<IJournalIterator> Unformated::get_journal()
{
    throw std::runtime_error("can't access journal - filesystem is not formated");
}

}
