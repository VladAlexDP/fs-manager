#include "fat.hpp"

#include <iostream>

namespace fs::filesystem
{

FAT::FAT(const std::string &part_path)
    : m_part_path(part_path)
{
}

entry::Directory FAT::mount(std::string_view)
{
    std::vector<std::unique_ptr<entry::IEntry>> root_entries;
    root_entries.emplace_back(new entry::Directory("/boot", std::vector<std::unique_ptr<entry::IEntry>>()));
    root_entries.emplace_back(new entry::Directory("/efi", std::vector<std::unique_ptr<entry::IEntry>>()));
    return entry::Directory("/", std::move(root_entries));
}

std::unique_ptr<IJournalIterator> FAT::get_journal()
{
    throw std::runtime_error("can't access journal - fat is not journaling filesystem");
}

}
