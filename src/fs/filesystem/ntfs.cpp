#include "ntfs.hpp"

#include <iostream>

namespace fs::filesystem
{

NTFS::NTFS(const std::string &part_path)
    : m_part_path(part_path)
{
}

entry::Directory NTFS::mount(std::string_view)
{
    std::vector<std::unique_ptr<entry::IEntry>> root_entries;
    root_entries.emplace_back(new entry::Directory("\\AppData", std::vector<std::unique_ptr<entry::IEntry>>()));
    root_entries.emplace_back(new entry::Directory("\\System", std::vector<std::unique_ptr<entry::IEntry>>()));
    root_entries.emplace_back(new entry::Directory("\\Users", std::vector<std::unique_ptr<entry::IEntry>>()));
    return entry::Directory("\\", std::move(root_entries));
}

std::unique_ptr<IJournalIterator> NTFS::get_journal()
{
    throw std::runtime_error("can't access journal - ntfs journal is not supported");
}

}
