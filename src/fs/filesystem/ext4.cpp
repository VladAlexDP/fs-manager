#include "ext4.hpp"

#include <iostream>

#include "fs/journal_iterator/ext4_journal_iterator.hpp"

#include "entry/file.hpp"

namespace fs::filesystem
{

EXT4::EXT4(const std::string &part_path)
    : m_part_path(part_path), m_journal()
{
    m_journal.push_back(std::make_unique<fs::journal_entry::EXT4JournalEntry>("create_file 10 12 ok"));
    m_journal.push_back(std::make_unique<fs::journal_entry::EXT4JournalEntry>("delete_file 10 12 ok"));
    m_journal.push_back(std::make_unique<fs::journal_entry::EXT4JournalEntry>("create_file 14 28 ok"));
    m_journal.push_back(std::make_unique<fs::journal_entry::EXT4JournalEntry>("write 14 28 failed"));
}

entry::Directory EXT4::mount(std::string_view)
{
    std::vector<std::unique_ptr<entry::IEntry>> root_entries;

    std::unique_ptr<entry::Directory> bin =
            std::make_unique<entry::Directory>("/bin", std::vector<std::unique_ptr<entry::IEntry>>());
    bin->add_child(std::make_unique<entry::File>("cat", 32));
    bin->add_child(std::make_unique<entry::File>("grep", 32));
    bin->add_child(std::make_unique<entry::File>("gcc", 512));
    bin->add_child(std::make_unique<entry::File>("sh", 256));
    root_entries.emplace_back(std::move(bin));

    root_entries.emplace_back(new entry::Directory("/etc", std::vector<std::unique_ptr<entry::IEntry>>()));

    std::unique_ptr<entry::Directory> home =
            std::make_unique<entry::Directory>("/home", std::vector<std::unique_ptr<entry::IEntry>>());
    std::unique_ptr<entry::Directory> home_vlad =
            std::make_unique<entry::Directory>("/home/vlad", std::vector<std::unique_ptr<entry::IEntry>>());
    home_vlad->add_child(std::make_unique<entry::File>("funny_video.mkv", 4096));
    home_vlad->add_child(std::make_unique<entry::File>("patterns.pdf", 13468));
    home->add_child(std::move(home_vlad));
    root_entries.emplace_back(std::move(home));

    return entry::Directory("/", std::move(root_entries));
}

std::unique_ptr<IJournalIterator> EXT4::get_journal()
{
    return std::make_unique<fs::journal_iterator::EXT4JournalIterator>(std::begin(m_journal),
                                                                       std::end(m_journal));
}

}
