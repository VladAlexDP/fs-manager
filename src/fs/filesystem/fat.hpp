#pragma once

#include "fs/filesystem.hpp"

namespace fs::filesystem
{

class FAT: public IFilesystem
{
public:
    explicit FAT(const std::string &part_path);

    entry::Directory mount(std::string_view path) override;
    std::unique_ptr<IJournalIterator> get_journal() override;

private:
    std::string m_part_path;
};

}
