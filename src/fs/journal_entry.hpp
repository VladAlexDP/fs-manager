#pragma once

#include <string_view>

namespace fs
{

class JournalEntry
{
public:
    virtual ~JournalEntry() = default;

    virtual std::string action() const = 0;
    virtual uint64_t first_block() const = 0;
    virtual uint64_t last_block() const = 0;
    virtual bool successful() const = 0;
};

}
