#pragma once

#include <memory>
#include <vector>

#include "fs/journal_entry/ext4_journal_entry.hpp"

#include "fs/journal_iterator.hpp"

namespace fs::journal_iterator
{

class EXT4JournalIterator: public IJournalIterator
{
public:
    EXT4JournalIterator(std::vector<std::unique_ptr<fs::journal_entry::EXT4JournalEntry>>::iterator begin,
                        std::vector<std::unique_ptr<fs::journal_entry::EXT4JournalEntry>>::iterator end);

    JournalEntry *next() override;
    bool has_next() override;

private:
    std::vector<std::unique_ptr<fs::journal_entry::EXT4JournalEntry>>::iterator m_current;
    std::vector<std::unique_ptr<fs::journal_entry::EXT4JournalEntry>>::iterator m_end;
};

}
