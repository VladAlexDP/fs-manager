#include "ext4_journal_iterator.hpp"

namespace fs::journal_iterator
{

EXT4JournalIterator::EXT4JournalIterator(std::vector<std::unique_ptr<fs::journal_entry::EXT4JournalEntry>>::iterator begin,
                                         std::vector<std::unique_ptr<fs::journal_entry::EXT4JournalEntry>>::iterator end)
    : m_current(begin), m_end(end)
{
}

JournalEntry *EXT4JournalIterator::next()
{
    auto current = m_current;
    m_current += 1;
    return current->get();
}

bool EXT4JournalIterator::has_next()
{
    return m_current != m_end;
}

}
