#pragma once

#include "journal_entry.hpp"

namespace fs
{

class IJournalIterator
{
public:
    virtual ~IJournalIterator() = default;

    virtual JournalEntry *next() = 0;
    virtual bool has_next() = 0;
};

}
