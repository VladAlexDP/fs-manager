#include <iostream>
#include <stack>

#include "block_layer/block_device.hpp"
#include "ui/ui_state.hpp"
#include "ui/ui_state/choose_device.hpp"

std::vector<block_layer::BlockDevice> scan_block_devices();

std::vector<block_layer::BlockDevice> scan_block_devices()
{
    std::vector<block_layer::BlockDevice> devices;
    devices.emplace_back("/dev/sda");
    devices.emplace_back("/dev/sdb");
    devices.emplace_back("/dev/sdc");
    return devices;
}

int main(int, char *[])
{
    auto block_devices = scan_block_devices();

    std::stack<std::unique_ptr<ui::IUIState>> states;
    states.emplace(std::make_unique<ui::ui_state::ChooseDevice>(block_devices));
    ui::IUIState *current_state = nullptr;

    while (true) {
        current_state = states.top().get();

        try {
            auto next = current_state->handle();
            if (next != nullptr && next.get() != current_state) {
                states.push(std::move(next));
            } else {
                states.pop();
            }
        } catch(const std::runtime_error &err) {
            std::cout << err.what() << std::endl;
        }
    }
    return 0;
}
