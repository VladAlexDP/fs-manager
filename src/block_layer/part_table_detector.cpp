#include "part_table_detector.hpp"

#include <stdexcept>

#include "part_table/gpt.hpp"
#include "part_table/mbr.hpp"
#include "part_table/no_table.hpp"

namespace block_layer
{

IPartTableDetector::IPartTableDetector(const std::string &blockdev_path)
    : m_candidate(std::make_unique<part_table::GPT>(blockdev_path)),
      m_successor(
            std::make_unique<IPartTableDetector>(
                std::make_unique<part_table::MBR>(blockdev_path),
                std::make_unique<IPartTableDetector>(
                    std::make_unique<part_table::NoTable>(),
                    nullptr
                )
            )
      )
{
}

std::unique_ptr<IPartTable> IPartTableDetector::detect()
{
    if (m_candidate->detect()) {
        std::unique_ptr<IPartTable> result;
        result.swap(m_candidate);
        return result;
    }

    if (m_successor != nullptr) {
        return m_successor->detect();
    }

    throw std::runtime_error("unsupported partition table");
}

IPartTableDetector::IPartTableDetector(std::unique_ptr<IPartTable> candidate, std::unique_ptr<IPartTableDetector> successor)
    : m_candidate(std::move(candidate)), m_successor(std::move(successor))
{
}

}
