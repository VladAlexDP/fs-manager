#pragma once

#include <memory>
#include <string>

#include "fs/filesystem.hpp"
#include "fs/filesystem_ops.hpp"

namespace block_layer
{

class Partition
{
public:
    explicit Partition(const std::string &path);

    std::string_view get_path() const;
    fs::IFilesystem *get_fs();
    void format(std::unique_ptr<fs::IFilesystemOps> fs_ops);

private:
    std::string m_path;
    std::unique_ptr<fs::IFilesystem> m_filesystem;
};

}
