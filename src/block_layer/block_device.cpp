#include "block_device.hpp"

#include <algorithm>

#include "part_table_detector.hpp"

namespace block_layer
{

BlockDevice::BlockDevice(const std::string &path)
    : m_path(path), m_partitions(), m_part_table()
{
    m_part_table = IPartTableDetector(m_path).detect();
    m_partitions = m_part_table->scan_partitions();
}

std::string_view BlockDevice::get_path() const
{
    return m_path;
}

std::vector<Partition>::iterator BlockDevice::begin()
{
    return m_partitions.begin();
}

std::vector<Partition>::iterator BlockDevice::end()
{
    return m_partitions.end();
}

void BlockDevice::add_partition(uint64_t start, uint64_t size)
{
    m_part_table->add_partition(start, size);
    m_partitions = m_part_table->scan_partitions();
}

void BlockDevice::remove_partition(const Partition &part)
{
    auto it = std::find_if(std::begin(m_partitions), std::end(m_partitions),
                           [&](auto &x) {
                               return x.get_path() == part.get_path();
                           });
    m_partitions.erase(it);
    m_partitions = m_part_table->scan_partitions();
}

}
