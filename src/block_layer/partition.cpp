#include "partition.hpp"

#include "fs/filesystem_detector.hpp"

namespace block_layer
{

Partition::Partition(const std::string &path)
    : m_path(path), m_filesystem(fs::FilesystemDetector(path).detect())
{
}

std::string_view Partition::get_path() const
{
    return m_path;
}

fs::IFilesystem *Partition::get_fs()
{
    return m_filesystem.get();
}

void Partition::format(std::unique_ptr<fs::IFilesystemOps> fs_ops)
{
    m_filesystem = fs_ops->format();
}

}
