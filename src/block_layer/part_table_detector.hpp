#pragma once

#include <memory>
#include <optional>
#include <string_view>

#include "part_table.hpp"

namespace block_layer
{

class IPartTableDetector
{
public:
    IPartTableDetector(const std::string &blockdev_path);
    IPartTableDetector(std::unique_ptr<IPartTable> candidate, std::unique_ptr<IPartTableDetector> successor);

    std::unique_ptr<IPartTable> detect();

private:
    std::unique_ptr<IPartTable> m_candidate;
    std::unique_ptr<IPartTableDetector> m_successor;
};

}
