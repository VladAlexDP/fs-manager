#pragma once

#include <memory>
#include <string>
#include <string_view>
#include <vector>

#include "part_table.hpp"
#include "partition.hpp"

namespace block_layer
{

class BlockDevice
{
public:
    explicit BlockDevice(const std::string &path);

    std::string_view get_path() const;
    std::vector<Partition>::iterator begin();
    std::vector<Partition>::iterator end();
    void add_partition(uint64_t start, uint64_t size);
    void remove_partition(const Partition &part);

private:
    std::string m_path;
    std::vector<Partition> m_partitions;
    std::unique_ptr<IPartTable> m_part_table;
};

}
