#pragma once

#include "block_layer/part_table.hpp"

namespace block_layer::part_table
{

class NoTable: public IPartTable
{
public:
    bool detect() const override;
    std::vector<Partition> scan_partitions() const override;
    void add_partition(int start, int size) override;
    void remove_partition(const Partition &part) override;
};

}
