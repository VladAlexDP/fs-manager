#include "gpt.hpp"

namespace block_layer::part_table
{

GPT::GPT(const std::string &path)
    : m_path(path), m_part_count(2)
{
}

bool GPT::detect() const
{
    return m_path == "/dev/sda";
}

std::vector<Partition> GPT::scan_partitions() const
{
    std::vector<Partition> result;
    for (size_t i = 0; i < m_part_count; ++i) {
        result.emplace_back(m_path + std::to_string(i));
    }

    return result;
}

void GPT::add_partition(int, int)
{
    m_part_count += 1;
}

void GPT::remove_partition(const Partition &)
{
    if (m_part_count == 0) {
        throw std::runtime_error("no parititons to remove");
    }

    m_part_count -= 1;
}

}
