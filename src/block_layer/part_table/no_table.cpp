#include "no_table.hpp"

namespace block_layer::part_table
{

bool NoTable::detect() const
{
    return true;
}

std::vector<Partition> NoTable::scan_partitions() const
{
    return std::vector<Partition>();
}

void NoTable::add_partition(int, int)
{
    throw std::runtime_error("can't add parititon - no partition table");
}

void NoTable::remove_partition(const Partition &)
{
    throw std::runtime_error("can't remove parititon - no partition table");
}

}
