#pragma once

#include <vector>
#include <memory>

#include "partition.hpp"

namespace block_layer
{

class IPartTable
{
public:
    virtual ~IPartTable() = default;

    virtual bool detect() const = 0;
    virtual std::vector<Partition> scan_partitions() const = 0;
    virtual void add_partition(int start, int size) = 0;
    virtual void remove_partition(const Partition &part) = 0;
};

}
